/*
 * Copyright 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package info.nicolaspanti.utils;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public final class CommandLineArgumentsUtilsTests
{
	@Test
	public void
	isHelpArgument()
	{
		assertTrue(CommandLineArgumentsUtils.isHelpArgument("-h"));
		assertTrue(CommandLineArgumentsUtils.isHelpArgument("--help"));
		
		assertFalse(CommandLineArgumentsUtils.isHelpArgument("--n"));
		assertFalse(CommandLineArgumentsUtils.isHelpArgument("--no"));
		assertFalse(CommandLineArgumentsUtils.isHelpArgument("--no-help"));
		
		assertFalse(CommandLineArgumentsUtils.isHelpArgument(null));
		assertFalse(CommandLineArgumentsUtils.isHelpArgument(""));
	}
	
	@Test
	public void
	isNextArgumentValid()
	{
		assertTrue(CommandLineArgumentsUtils.isNextArgumentValid(
				0, new String[] {"--set", "5"}));
		assertTrue(CommandLineArgumentsUtils.isNextArgumentValid(
				0, new String[] {"--set-value", "5"}));
		assertTrue(CommandLineArgumentsUtils.isNextArgumentValid(
				0, new String[] {"--set-value", "toto"}));
		assertTrue(CommandLineArgumentsUtils.isNextArgumentValid(
				1, new String[] {"--verbose", "--set", "5"}));
		assertTrue(CommandLineArgumentsUtils.isNextArgumentValid(
				1, new String[] {"--verbose", "--set", "5", "--quiet"}));

		assertFalse(CommandLineArgumentsUtils.isNextArgumentValid(
				-1, new String[] {"--set-value", "toto"}));
		assertFalse(CommandLineArgumentsUtils.isNextArgumentValid(
				0, null));
		assertFalse(CommandLineArgumentsUtils.isNextArgumentValid(
				0, new String[] {"--verbose"}));
		assertFalse(CommandLineArgumentsUtils.isNextArgumentValid(
				0, new String[] {"--set", null}));
		assertFalse(CommandLineArgumentsUtils.isNextArgumentValid(
				0, new String[] {"--set", ""}));
	}
}
