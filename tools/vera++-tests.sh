#!/bin/sh

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


# See https://bitbucket.org/verateam/vera/wiki/Home

EXIT_SUCCESS=0
EXIT_FAILURE=1

cmd='vera++ --error'
cmd="$cmd -R F001" # source files should not use the '\r' (CR) character
cmd="$cmd -R F002 -P max-filename-length=40"
cmd="$cmd -R L004 -P max-line-length=90"
cmd="$cmd -R L005 -P max-consecutive-empty-lines=2"
cmd="$cmd -R L006 -P max-file-length=600"

exit_code=$EXIT_SUCCESS
for file_or_dir in "$@"
do
    if test ! -e "$file_or_dir"
    then
	>&2 echo "$file_or_dir does not exist!"
	exit_code=$EXIT_FAILURE
    else
	files=$file_or_dir
	if test -d $files
	then
	    files=$(find "$file_or_dir" \( \
	            -name '*.h' -o -name '*.c' \
		    -name '*.hh' -o -name '*.hpp' -o -name '*.hxx' -o \
		    -name '*.cc' -o -name '*.cpp' -o -name '*.cxx' -o \
	            -name '*.java' -o -name '*.js' -o -name '*.php' -o \
		    -name '*.py' -o -name '*.sh' -o -name '*.m' -o \
	            -name '.*config' -o -name '*.el' \))
	fi
	if test "$files" != ''
	then
	    echo "$files" | $cmd
	    if test $? -ne $EXIT_SUCCESS
	    then
		exit_code=$EXIT_FAILURE
	    fi
	fi
    fi
done
exit $exit_code
