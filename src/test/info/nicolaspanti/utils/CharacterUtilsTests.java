/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package info.nicolaspanti.utils;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public final class CharacterUtilsTests
{
	@Test
	public void
	isDigit()
	{
		for(char i = '0'; i <= '9'; ++i)
		{
			assertTrue(CharacterUtils.isDigit(i));
		}

		assertFalse(CharacterUtils.isDigit('+'));
		assertFalse(CharacterUtils.isDigit('-'));
		assertFalse(CharacterUtils.isDigit('*'));
		assertFalse(CharacterUtils.isDigit('/'));
		assertFalse(CharacterUtils.isDigit('_'));
	}
	
	@Test
	public void
	toByte()
	{
		for(char i = 0; i < 10; ++i)
		{
			assertEquals(CharacterUtils.toByte((char)('0' + i)), i);
		}
	}
}
