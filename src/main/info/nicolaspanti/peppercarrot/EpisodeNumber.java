/*
 * Copyright (C) 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot;


import java.io.PrintStream;
import java.util.Scanner;


public final class EpisodeNumber
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private EpisodeNumber()
	{}
	
	
	public static final short ALL   = -1;
	public static final short ERROR = -2;
	
	
	public static
	void
	askOnStream(final PrintStream stream)
	{
		stream.print("Enter the number of the episode (with digit(s)): ");
		stream.flush();
	}
	
	public static
	void
	askOnSystemOut()
	{
		askOnStream(System.out);
	}
	
	public static
	String
	getNextLineFromScanner(final Scanner scanner)
	{
		if(scanner == null)
		{
			return null;
		}
		return scanner.nextLine().replace(" ", "");
	}
	
	public static
	short
	getFromString(final String string, final PrintStream errorStream)
	{
		if(string == null || string.isEmpty())
		{
			return ERROR;
		}
		
		try
		{
			final short number = Short.parseShort(string);
			if(number < 0 && number != ALL)
			{
				if(errorStream != null)
				{
					System.err.println("The number of the episode must be positive.");
					System.err.println(
							"The only exception is "+ ALL +
							" for all episodes.");
					System.err.flush();
				}
				return ERROR;
			}
			return number;
		}
		catch(final NumberFormatException e)
		{
			if(errorStream != null)
			{
				System.err.println("You must enter a number with one or more digits.");
				System.err.flush();
			}
			return ERROR;
		}
	}
	
	public static
	short
	getFromString(final String string)
	{
		return getFromString(string, System.err);
	}
	
	public static
	short
	askAndGet(final Scanner scanner)
	{
		if(scanner == null)
		{
			return ERROR;
		}
		EpisodeNumber.askOnSystemOut();
		final String line = EpisodeNumber.getNextLineFromScanner(scanner);
		return EpisodeNumber.getFromString(line);
	}
	
	public static
	short
	askAndGetACorrectOne(final Scanner scanner)
	{
		short episodeNumber;
		do
		{
			episodeNumber = EpisodeNumber.askAndGet(scanner);
		}
		while(episodeNumber != EpisodeNumber.ERROR);
		return episodeNumber;
	}
}
