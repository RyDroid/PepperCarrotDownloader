/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package info.nicolaspanti.utils;


public final class CharacterUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private CharacterUtils()
	{}
	
	
	public static
	boolean
	isDigit(char character)
	{
		return character >= '0' && character <= '9';
	}
	
	public static
	byte
	toByte(char character)
	{
		return (byte) (character - '0');
	}
}
