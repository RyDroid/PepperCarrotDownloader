/*
 * Copyright (C) 2016-2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot.download;


import info.nicolaspanti.peppercarrot.Episode;
import info.nicolaspanti.peppercarrot.ImageQuality;
import info.nicolaspanti.utils.DownloadUtils;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;


public class EpisodeDownloader
{
	public static final String DEFAULT_BASE_URL_STRING =
			"https://www.peppercarrot.com/0_sources/";
	public static final ImageQuality DEFAULT_QUALITY =
			ImageQuality.HIGH;
	public static final String DEFAULT_FILE_EXTENSION =
			"jpg";
	public static final String DEFAULT_LANGUAGE =
			"en";
	
	
	public static final
	boolean
	askLanguageFromStream(final PrintStream stream)
	{
		if(stream == null)
		{
			return false;
		}
		stream.print("Enter the language: ");
		stream.flush();
		return true;
	}
	
	public static final
	void
	askLanguageFromSystemOut()
	{
		askLanguageFromStream(System.out);
	}
	
	
	private String       _language;
	private ImageQuality _quality;
	private String       _baseUrlString;
	private String       _fileExtension;

	
	public EpisodeDownloader(
			final String language,
			final ImageQuality quality,
			final String baseUrlString,
			final String fileExtension)
	{
		_language      = language;
		_quality       = quality;
		_baseUrlString = baseUrlString;
		_fileExtension = fileExtension;
	}
	
	public EpisodeDownloader(
			final String language,
			final ImageQuality quality)
	{
		this(language, quality, DEFAULT_BASE_URL_STRING, DEFAULT_FILE_EXTENSION);
	}
	
	public EpisodeDownloader(final String language)
	{
		this(language, DEFAULT_QUALITY);
	}
	
	public EpisodeDownloader()
	{
		this(DEFAULT_LANGUAGE);
	}
	
	
	public
	String
	getLanguage()
	{
		return _language;
	}
	
	public
	boolean
	setLanguage(final String language)
	{
		if(language == null || language.isEmpty())
		{
			return false;
		}
		_language = language;
		return true;
	}
	
	public
	boolean
	setFromScanner(final Scanner scanner)
	{
		if(scanner == null)
		{
			return false;
		}
		if(!setLanguage(scanner.nextLine()))
		{
			System.err.println("The language was not recognized.");
			System.err.flush();
			System.out.println("The language is "+ getLanguage() +".");
			System.out.flush();
			return false;
		}
		return true;
	}
	
	public
	boolean
	askAndSetLanguage(final Scanner scanner, final PrintStream stream)
	{
		return askLanguageFromStream(stream) && setFromScanner(scanner);
	}
	
	public
	boolean
	askAndSetLanguage(final Scanner scanner)
	{
		return askAndSetLanguage(scanner, System.out);
	}
	
	public
	boolean
	askAndSetLanguage(final InputStream inputStream)
	{
		final Scanner scanner = new Scanner(inputStream);
		final boolean result = askAndSetLanguage(scanner);
		scanner.close();
		return result;
	}
	
	public
	boolean
	askAndSetLanguage()
	{
		return askAndSetLanguage(System.in);
	}
	
	
	public
	ImageQuality
	getQuality()
	{
		return _quality;
	}
	
	public
	String
	getQualityString()
	{
		if(_quality == null)
		{
			return null;
		}
		return _quality.toString();
	}
	
	public
	boolean
	setQuality(final ImageQuality quality)
	{
		if(quality == null)
		{
			return false;
		}
		_quality = quality;
		return true;
	}
	
	public
	boolean
	setQualityWithString(final String quality)
	{
		return setQuality(ImageQuality.parseString(quality));
	}
	
	public
	String
	getBaseUrlString()
	{
		return _baseUrlString;
	}
	
	public
	String
	getFileExtension()
	{
		return _fileExtension;
	}
	
	
	public
	String
	getFolderUrlString(final Episode episode)
	{
		if(_baseUrlString == null || episode == null || _quality == null)
		{
			return null;
		}
		return
				_baseUrlString +
				episode.toDefaultIdentifier() + "/" +
				_quality.toString() + "/";
	}
	
	public
	String
	getFileNameWithoutExtension(final Episode episode)
	{
		if(_language == null || episode == null)
		{
			return null;
		}
		return
				_language +
				"_Pepper-and-Carrot_by-David-Revoy_" +
				"E" + String.format("%02d", episode.getNumber());
	}
	
	public
	String
	getCoverFileNameWithoutExtension(final Episode episode)
	{
		return getFileNameWithoutExtension(episode);
	}
	
	public
	String
	getPartFileNameWithoutExtension(final Episode episode, byte part)
	{
		final String fileName = getFileNameWithoutExtension(episode);
		if(fileName == null)
		{
			return null;
		}
		return fileName + "P" + String.format("%02d", part);
	}
	
	public
	String
	getCoverUrlWithoutExtensionString(final Episode episode)
	{
		final String folder = getFolderUrlString(episode);
		if(folder == null)
		{
			return null;
		}
		
		final String file   = getCoverFileNameWithoutExtension(episode);
		if(file == null)
		{
			return null;
		}
		
		return folder + file;
	}
	
	public
	String
	getPartUrlWithoutExtensionString(final Episode episode, byte part)
	{
		final String folder = getFolderUrlString(episode);
		if(folder == null)
		{
			return null;
		}
		
		final String file   = getPartFileNameWithoutExtension(episode, part);
		if(file == null)
		{
			return null;
		}
		
		return folder + file;
	}
	
	public
	String
	getCoverUrlString(final Episode episode)
	{
		return getCoverUrlWithoutExtensionString(episode) + "." + _fileExtension;
	}
	
	public
	String
	getPartUrlString(final Episode episode, byte part)
	{
		return getPartUrlWithoutExtensionString(episode, part) + "." + _fileExtension;
	}
	
	public
	String
	getPartUrlString(final Episode episode, int part)
	{
		return getPartUrlString(episode, (byte) part);
	}

	
	public
	boolean
	downloadCover(final Episode episode)
	{
		return DownloadUtils.downloadFileWithUrlString(
				getCoverUrlString(episode));
	}
	
	public
	boolean
	downloadPart(final Episode episode, byte part)
	{
		return DownloadUtils.downloadFileWithUrlString(
				getPartUrlString(episode, part));
	}
	
	public
	byte
	downloadParts(final Episode episode)
	{
		byte part;
		for(part=0; part < Byte.MAX_VALUE; ++part)
		{
			if(!downloadPart(episode, part))
			{
				break;
			}
		}
		return part;
	}
	
	public
	byte
	download(final Episode episode)
	{
		byte nb;
		if(downloadCover(episode))
		{
			nb = 1;
		}
		else
		{
			nb = 0;
		}
		nb += downloadParts(episode);
		return nb;
	}
}
