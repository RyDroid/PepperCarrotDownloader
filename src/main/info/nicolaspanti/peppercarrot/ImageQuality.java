/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package info.nicolaspanti.peppercarrot;


public enum ImageQuality
{
	LOW,
	HIGH;
	
	
	public
	String
	toString()
	{
		if(this == LOW)
		{
			return "low-res";
		}
		if(this == HIGH)
		{
			return "hi-res";
		}
		return null;
	}

	
	public static
	ImageQuality
	parseCharacter(char character)
	{
		if(character == 'l' || character == 'L')
		{
			return LOW;
		}
		if(character == 'h' || character == 'H')
		{
			return HIGH;
		}
		return null;
	}
	
	private static
	ImageQuality
	parseStringTrimedLoweredNotEmpty(final String string)
	{
		if(string.length() == 1)
		{
			return parseCharacter(string.charAt(0));
		}
		
		if(string.equals("low") || string.equals("low-res"))
		{
			return LOW;
		}
		if(string.equals("high") || string.equals("hi-res") || string.equals("high-res"))
		{
			return HIGH;
		}
		return null;
	}
	
	private static
	ImageQuality
	parseStringUnsafe(String string)
	{
		string = string.trim().toLowerCase();
		if(string.isEmpty())
		{
			return null;
		}
		return parseStringTrimedLoweredNotEmpty(string);
	}
	
	public static
	ImageQuality
	parseString(final String string)
	{
		if(string == null)
		{
			return null;
		}
		return parseStringUnsafe(string);
	}
}
