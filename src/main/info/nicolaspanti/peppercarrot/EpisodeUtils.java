/*
 * Copyright (C) 2016-2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot;


import info.nicolaspanti.utils.CharacterUtils;


public final class EpisodeUtils
{
	public static final short MIN_IDENTIFIER_LENGTH = 4;
	
	
	/**
	 * Don't let anyone instantiate this class.
	 */
	private EpisodeUtils()
	{}
	
	
	private static
	Episode
	parseDefaultStartsCorrectlyWithDigit(String id)
	{
		char tmp = id.charAt(0);
		short number = CharacterUtils.toByte(tmp);
		id = id.substring(1);
		tmp = id.charAt(0);
		while(CharacterUtils.isDigit(tmp))
		{
			number = (short)(number * 10 + CharacterUtils.toByte(tmp));
			id = id.substring(1);
			if(id.isEmpty())
			{
				return null;
			}
			tmp = id.charAt(0);
		}
		
		if(id.isEmpty())
		{
			return null;
		}
		tmp = id.charAt(0);
		while(tmp == ' ' || tmp == '_')
		{
			id = id.substring(1);
			if(id.isEmpty())
			{
				return null;
			}
			tmp = id.charAt(0);
		}
		
		if(id.isEmpty())
		{
			return null;
		}
		final String name = id;
		
		return new Episode(number, name);
	}
	
	private static
	Episode
	parseDefaultStartsCorrectly(final String id)
	{
		if(!CharacterUtils.isDigit(id.charAt(0)))
		{
			return null;
		}
		return parseDefaultStartsCorrectlyWithDigit(id);
	}
	
	public static
	Episode
	parseDefault(final String id)
	{
		if(id == null)
		{
			return null;
		}
		if(!id.toLowerCase().startsWith("ep") || id.length() < MIN_IDENTIFIER_LENGTH)
		{
			return null;
		}
		return parseDefaultStartsCorrectly(id.substring(2));
	}
}
