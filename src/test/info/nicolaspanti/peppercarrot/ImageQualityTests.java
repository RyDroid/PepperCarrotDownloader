/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package info.nicolaspanti.peppercarrot;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public final class ImageQualityTests
{
	@Test
	public void
	compare()
	{
		assertTrue(ImageQuality.LOW.compareTo(ImageQuality.HIGH) < 0);
		assertTrue(ImageQuality.HIGH.compareTo(ImageQuality.LOW) > 0);
	}
	
	@Test
	public void
	parseCharacter()
	{
		assertEquals(ImageQuality.LOW, ImageQuality.parseCharacter('l'));
		assertEquals(ImageQuality.LOW, ImageQuality.parseCharacter('L'));
		
		assertEquals(ImageQuality.HIGH, ImageQuality.parseCharacter('h'));
		assertEquals(ImageQuality.HIGH, ImageQuality.parseCharacter('H'));

		assertNull(ImageQuality.parseCharacter('\0'));
		assertNull(ImageQuality.parseCharacter('\n'));
		assertNull(ImageQuality.parseCharacter('\r'));
		assertNull(ImageQuality.parseCharacter('\t'));
	}
	
	@Test
	public void
	parseString()
	{
		assertNotNull(ImageQuality.parseString(ImageQuality.LOW.toString()));
		assertNotNull(ImageQuality.parseString(ImageQuality.HIGH.toString()));
		
		assertEquals(
				ImageQuality.parseString(ImageQuality.LOW.toString()),
				ImageQuality.LOW);
		assertEquals(
				ImageQuality.parseString(ImageQuality.HIGH.toString()),
				ImageQuality.HIGH);
		
		assertEquals(
				ImageQuality.parseString("l"),
				ImageQuality.LOW);
		assertEquals(
				ImageQuality.parseString("L"),
				ImageQuality.LOW);
		assertEquals(
				ImageQuality.parseString("low"),
				ImageQuality.LOW);
		assertEquals(
				ImageQuality.parseString("Low"),
				ImageQuality.LOW);
		assertEquals(
				ImageQuality.parseString("LOW"),
				ImageQuality.LOW);
		assertEquals(
				ImageQuality.parseString("low-res"),
				ImageQuality.LOW);
		
		assertEquals(
				ImageQuality.parseString("h"),
				ImageQuality.HIGH);
		assertEquals(
				ImageQuality.parseString("H"),
				ImageQuality.HIGH);
		assertEquals(
				ImageQuality.parseString("high"),
				ImageQuality.HIGH);
		assertEquals(
				ImageQuality.parseString("High"),
				ImageQuality.HIGH);
		assertEquals(
				ImageQuality.parseString("HIGH"),
				ImageQuality.HIGH);
		assertEquals(
				ImageQuality.parseString("hi-res"),
				ImageQuality.HIGH);
		assertEquals(
				ImageQuality.parseString("high-res"),
				ImageQuality.HIGH);
		
		assertNull(ImageQuality.parseString(null));
		assertNull(ImageQuality.parseString(""));
	}
}
