/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot.download;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import info.nicolaspanti.peppercarrot.Episode;

import org.junit.Test;


public final class EpisodeDownloaderTests
{
	public static final EpisodeDownloader DOWNLOADER =
			new EpisodeDownloader();
	public static final Episode EP1 =
			new Episode(1, "Potion-of-Flight");

	
	@Test
	public void
	defaultBaseUrl()
	{
		assertTrue(EpisodeDownloader.DEFAULT_BASE_URL_STRING.endsWith("/")
				|| EpisodeDownloader.DEFAULT_BASE_URL_STRING.isEmpty());
	}
	
	@Test
	public void
	getQuality()
	{
		assertNotNull(DOWNLOADER.getQuality());
	}
	
	@Test
	public void
	getQualityString()
	{
		assertNotNull(DOWNLOADER.getQualityString());
		assertEquals(
				DOWNLOADER.getQuality().toString(),
				DOWNLOADER.getQualityString());
	}
	
	@Test
	public void
	setQuality()
	{
		assertFalse(new EpisodeDownloader().setQuality(null));
	}
	
	@Test
	public void
	setQualityWithString()
	{
		assertFalse(new EpisodeDownloader().setQualityWithString(null));
		assertFalse(new EpisodeDownloader().setQualityWithString(""));

		assertTrue(new EpisodeDownloader().setQualityWithString("l"));
		assertTrue(new EpisodeDownloader().setQualityWithString("L"));
		assertTrue(new EpisodeDownloader().setQualityWithString("low"));
		assertTrue(new EpisodeDownloader().setQualityWithString("LOW"));
		assertTrue(new EpisodeDownloader().setQualityWithString("low-res"));
		
		assertTrue(new EpisodeDownloader().setQualityWithString("h"));
		assertTrue(new EpisodeDownloader().setQualityWithString("H"));
		assertTrue(new EpisodeDownloader().setQualityWithString("high"));
		assertTrue(new EpisodeDownloader().setQualityWithString("HIGH"));
		assertTrue(new EpisodeDownloader().setQualityWithString("hi-res"));
		assertTrue(new EpisodeDownloader().setQualityWithString("high-res"));
	}

	@Test
	public void
	getFolderUrlString()
	{
		assertEquals(
				DOWNLOADER.getFolderUrlString(EP1),
				DOWNLOADER.getBaseUrlString() +
				EP1.toDefaultIdentifier() +"/" +
				DOWNLOADER.getQualityString() + "/");
	}
	
	@Test
	public void
	getCoverUrlString()
	{
		assertEquals(
				DOWNLOADER.getCoverUrlString(EP1),
				DOWNLOADER.getBaseUrlString() +
				EP1.toDefaultIdentifier() + "/" +
				DOWNLOADER.getQualityString() +
				"/en_Pepper-and-Carrot_by-David-Revoy_E01."+
				DOWNLOADER.getFileExtension());
	}
	
	@Test
	public void
	getPartUrlString()
	{
		assertEquals(
				DOWNLOADER.getPartUrlString(EP1, 0),
				DOWNLOADER.getBaseUrlString() +
				EP1.toDefaultIdentifier() + "/" +
				DOWNLOADER.getQualityString() +
				"/en_Pepper-and-Carrot_by-David-Revoy_E01P00."+
				DOWNLOADER.getFileExtension());
	}
}
