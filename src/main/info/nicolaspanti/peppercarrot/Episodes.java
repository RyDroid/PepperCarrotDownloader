/*
 * Copyright (C) 2016-2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;


public final class Episodes
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private Episodes()
	{}
	
	
	public static final String DEFAULT_URL_STRING =
			"https://www.peppercarrot.com/0_sources/.episodes-list.md";
	
	
	public static
	Collection<Episode>
	getCollectionFromInputStream(final InputStream stream)
	{
		if(stream == null)
		{
			return null;
		}
		
		final Collection<Episode> episodes =
				new ArrayList<Episode>();
        final BufferedReader bufferReader =
        		new BufferedReader(new InputStreamReader(stream));
        String line;
        
        try
        {
			while((line = bufferReader.readLine()) != null)
			{
				final Episode ep = EpisodeUtils.parseDefault(line);
				if(ep == null)
				{
					return null;
				}
				episodes.add(ep);
			}
			bufferReader.close();
		}
        catch(final IOException e)
		{
			e.printStackTrace();
			return null;
		}
        return episodes;
	}
	
	public static
	Collection<Episode>
	getCollectionFromUrl(final URL url)
	{
		if(url == null)
		{
			return null;
		}
		
		try
		{
			final InputStream stream = url.openStream();
			final Collection<Episode> episodes =
					getCollectionFromInputStream(stream);
			stream.close();
			return episodes;
		}
		catch(final IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static
	Collection<Episode>
	getCollectionFromUrlString(String url)
	{
		if(url == null || url.isEmpty())
		{
			return null;
		}
		
		try
		{
			return getCollectionFromUrl(new URL(url));
		}
		catch(final MalformedURLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static
	Collection<Episode>
	getCollectionFromDefaultUrl()
	{
		return getCollectionFromUrlString(DEFAULT_URL_STRING);
	}
	
	public static
	Episode[]
	getArrayFromDefaultUrl()
	{
		final Episode[] episodes = new Episode[0];
		final Collection<Episode> collection = getCollectionFromDefaultUrl();
		if(collection == null)
		{
			return null;
		}
		return collection.toArray(episodes);
	}
	
	public static
	Episode[]
	getArrayFromDefaultUrlOrExit(final PrintStream stream)
	{
		final Episode[] episodes = Episodes.getArrayFromDefaultUrl();
		if(episodes == null)
		{
			if(stream != null)
			{
				stream.println("List of episodes was not fetched!");
				stream.flush();
			}
			System.exit(1);
		}
		if(episodes.length == 0)
		{
			if(stream != null)
			{
				stream.println("List of episodes is empty!");
				stream.flush();
			}
			System.exit(1);
		}
		return episodes;
	}
	
	public static
	Episode[]
	getArrayFromDefaultUrlOrExit()
	{
		return getArrayFromDefaultUrlOrExit(System.err);
	}
}
