/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import info.nicolaspanti.utils.UrlUtils;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Test;


public final class EpisodesTests
{
	private static final short MIN_NUMBER_OF_EPISODES =  21;
	private static final short MAX_NUMBER_OF_EPISODES = 500;
	
	
	@Test
	public void
	defaultUrl()
	{
		UrlUtils.isWeb(Episodes.DEFAULT_URL_STRING);
	}
	
	@Test
	public void
	getCollectionFromUrl()
	{
		assertNull(Episodes.getCollectionFromUrl(null));
	}
	
	@Test
	public void
	getCollectionFromUrlString()
	{
		assertNull(Episodes.getCollectionFromUrlString(null));
		assertNull(Episodes.getCollectionFromUrlString(""));
	}
	
	@Test
	public void
	getCollectionFromDefaultUrl()
	{
		final Collection<Episode> episodes =
				Episodes.getCollectionFromDefaultUrl();
		assertNotNull(episodes);
		
		final short nb = (short) episodes.size();
		assertTrue(nb >= MIN_NUMBER_OF_EPISODES);
		assertTrue(nb <  MAX_NUMBER_OF_EPISODES);
		
		final Iterator<Episode> it = episodes.iterator();
		while(it.hasNext())
		{
			assertTrue(it.next().isValid());
		}
	}
}
