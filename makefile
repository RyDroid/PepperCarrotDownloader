# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


# Folders and files
SRC_DIR=src/main
CHK_DIR=src/test
DOC_DIR=doc

# Commands
KWSTYLE=KWStyle -R -v -gcc -d
JAVA=java
JAR=build/jar/PepperCarrotDownloader.jar
EXECUTABLE=$(JAVA) -classpath $(JAR) \
	info/nicolaspanti/peppercarrot/download/EpisodesDownloader

# Package and archive
PACKAGE=PepperCarrotDownloader
FILES_TO_BUILD=$(SRC_DIR)/* $(CHK_DIR)/* build.xml makefile
FILES_FOR_CI=.gitlab-ci.yml
FILES_FOR_TESTS=$(FILES_FOR_CI) KWStyle.xml
FILES_TXT_TO_ARCHIVE=\
	$(FILES_TO_BUILD) tools/* \
	.gitignore .editorconfig \
	$(FILES_FOR_TESTS) \
	*.md licenses/*
FILES_TO_ARCHIVE=$(FILES_TXT_TO_ARCHIVE) logo.png


all: default

default: build.xml
	ant


check: test

test: test-static test-dynamic

test-static: jar test-kwstyle test-vera++ test-encoding

test-kwstyle:
	$(KWSTYLE) $(SRC_DIR)
	$(KWSTYLE) $(CHK_DIR)

test-vera++:
	@chmod +x ./tools/vera++-tests.sh
	@! ./tools/vera++-tests.sh tools/.newline-no.txt 2> /dev/null
	./tools/vera++-tests.sh \
		$(SRC_DIR)/* $(CHK_DIR)/* \
		build.xml tools/*

test-encoding:
	@chmod +x ./tools/encoding-tests.sh
	@! ./tools/encoding-tests.sh \
		tools/.encoding_iso-8859-15.txt 2> /dev/null
	./tools/encoding-tests.sh $(FILES_TXT_TO_ARCHIVE)

test-checkstyle:
	checkstyle -c checkstyle.xml -r src/ -f plain

test-dynamic: build.xml test-junit test-use-cases

test-junit: build.xml
	ant junit

test-use-cases: jar
	$(EXECUTABLE) \
		-? > /dev/null
	$(EXECUTABLE) \
		-h > /dev/null
	$(EXECUTABLE) \
		--help > /dev/null
	$(EXECUTABLE) \
		--no-ask --verbose -e 1 --lang en
	$(EXECUTABLE) \
		--no-ask --quiet --episode 2 --language fr
	$(EXECUTABLE) \
		--no-ask -e 3 --lang en -q low-res
	$(EXECUTABLE) \
		--no-ask -e 4 --lang en -q hi-res


jar: build.xml
	ant jar

compile: build.xml
	ant compile


clean: \
	clean-ant clean-build clean-bin \
	clean-python clean-profiling clean-ide \
	clean-tmp clean-doc clean-latex clean-tests clean-archives

clean-ant: build.xml
	@ant clean

clean-build: clean-build-release clean-build-debug

clean-build-release:
	@$(RM) -rf -- release/ Release/

clean-build-debug:
	@$(RM) -rf -- dbg/ debug/ Debug/

clean-bin:
	@$(RM) -rf -- *.o *.a *.so *.ko *.lo *.dll *.out bin/

clean-python:
	@$(RM) -rf -- *.pyc __pycache__

clean-profiling:
	@$(RM) -rf -- callgrind.out.*

clean-ide: clean-qt-creator clean-codeblocks

clean-qt-creator:
	@$(RM) -f -- qmake_makefile *.pro.user

clean-codeblocks:
	@$(RM) -f -- *.cbp *.CBP

clean-tmp:
	@$(RM) -rf -- \
		*~ *.bak *.backup .\#* \#* \
		*.sav *.save *.autosav *.autosave \
		*.log *.log.* error_log* \
		.cache/ .thumbnails/

clean-doc:
	@$(RM) -rf -- \
		doc/ Doc/ docs/ Docs/ \
		documentation/ Documentation/

clean-latex:
	@$(RM) -f -- \
		*.pdf *.dvi *.ps \
		*.PDF *.DVI *.PS \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.gz *.synctex.bz2 *.synctex.xz \
		*.synctex.zip *.synctex.rar

clean-tests: clean-tests-files

clean-tests-files:
	@$(RM) -rf -- \
		a b c a.* b.* c.* \
		test.c tests.c \
		test.cc tests.cc test.cpp tests.cpp \
		test.java tests.java \
		test.py tests.py \
		junit* report* TEST*

clean-archives:
	@$(RM) -rf -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-git:
	git clean -fdx


run-command-line: build.xml
	ant run-command-line

run-gui: build.xml
	ant run-gui

run-spring: build.xml
	ant run-spring


archives: zip tar-gz tar-bz2 tar-xz 7z

dist: default-archive

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE)

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE)

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE)

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE)
