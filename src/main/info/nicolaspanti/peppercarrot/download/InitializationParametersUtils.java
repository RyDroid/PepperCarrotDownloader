/*
 * Copyright (C) 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot.download;


public final class InitializationParametersUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private InitializationParametersUtils()
	{}

	
	public static
	boolean
	isArgumentForEpisode(final String argument)
	{
		return argument != null &&
				(argument.equals("-e") || argument.equals("--ep") || argument.equals("--episode"));
	}
	
	public static
	boolean
	isArgumentForAllEpisodes(final String argument)
	{
		return argument != null &&
				(argument.equals("--all-eps") || argument.equals("--all-episodes"));
	}
	
	public static
	boolean
	isArgumentForLanguage(final String argument)
	{
		return argument != null &&
				(argument.equals("--lang") || argument.equals("--language"));
	}
	
	public static
	boolean
	isArgumentForQuality(final String argument)
	{
		return argument != null &&
				(argument.equals("-q") || argument.equals("--quality"));
	}
	
	public static
	boolean
	isArgumentForFormat(final String argument)
	{
		return argument != null &&
				(argument.equals("-f") || argument.equals("--format"));
	}
	
	public static
	boolean
	isArgumentForFolder(final String argument)
	{
		return argument != null && (
				argument.equals("-o") ||
				argument.equals("--output") ||
				argument.equals("--folder")
				);
	}
}
