/*
 * Copyright (C) 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot.download;


import info.nicolaspanti.peppercarrot.EpisodeNumber;
import info.nicolaspanti.peppercarrot.EpisodesProxy;
import info.nicolaspanti.utils.CommandLineArgumentsUtils;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;


public class InitializationParameters
{
	public boolean verbose      = false;
	public boolean ask          = true;
	public boolean listEpisodes = false;
	public short episodeNumber  = EpisodeNumber.ERROR;
	public String format        = null;
	public String folder        = null;
	public final EpisodeDownloader downloader = new EpisodeDownloader();

	
	public InitializationParameters()
	{
		EpisodesProxy.fetchOrExit();
	}
	
	public InitializationParameters(final String[] args)
	{
		this();
		set(args);
	}
	
	
	private
	int
	parseArgument(final String[] args, int i)
	{
		if(args[i].equals("--ask"))
		{
			ask = true;
		}
		else if(args[i].equals("--no-ask"))
		{
			ask = false;
		}
		else if(args[i].equals("--quiet"))
		{
			verbose = false;
		}
		else if(args[i].equals("--verbose"))
		{
			verbose = true;
		}
		else if(InitializationParametersUtils.isArgumentForEpisode(args[i]))
		{
			if(!CommandLineArgumentsUtils.isNextArgumentValid(i, args))
			{
				System.err.println(args[i] +" needs a value (with the following argument).");
				return -1;
			}
			
			++i;
			args[i] = args[i].replace(" ", "");
			try
			{
				episodeNumber = Short.parseShort(args[i]);
			}
			catch(final NumberFormatException e)
			{
				System.err.println(args[i] +" is not a valid episode number.");
				return -1;
			}
		}
		else if(InitializationParametersUtils.isArgumentForAllEpisodes(args[i]))
		{
			episodeNumber = EpisodeNumber.ALL;
		}
		else if(InitializationParametersUtils.isArgumentForLanguage(args[i]))
		{
			if(!CommandLineArgumentsUtils.isNextArgumentValid(i, args))
			{
				System.err.println(args[i] +" needs a value (with the following argument).");
				return -1;
			}
			
			if(!downloader.setLanguage(args[++i]))
			{
				System.err.println("Language "+ args[i] +" is not valid.");
				return -1;
			}
		}
		else if(InitializationParametersUtils.isArgumentForQuality(args[i]))
		{
			if(!CommandLineArgumentsUtils.isNextArgumentValid(i, args))
			{
				System.err.println(args[i] +" needs a value (with the following argument).");
				return -1;
			}
			
			if(!downloader.setQualityWithString(args[++i]))
			{
				System.err.println("Quality "+ args[i] +" is not valid.");
				return -1;
			}
		}
		else if(InitializationParametersUtils.isArgumentForFormat(args[i]))
		{
			if(!CommandLineArgumentsUtils.isNextArgumentValid(i, args))
			{
				System.err.println(args[i] +" needs a value (with the following argument).");
				System.exit(1);
			}

			format = args[++i];
			System.err.println("Option "+ args[i] +" is not managed yet.");
			return -1;
		}
		else if(InitializationParametersUtils.isArgumentForFolder(args[i]))
		{
			if(!CommandLineArgumentsUtils.isNextArgumentValid(i, args))
			{
				System.err.println(args[i] +" needs a value (with the following argument).");
				System.exit(1);
			}
			
			folder = args[++i];
			final File potentialFolder = new File(folder);
			if(!potentialFolder.exists())
			{
				System.err.println(folder +" is not a file/folder that exists.");
				System.exit(1);
			}
			if(!potentialFolder.isDirectory())
			{
				System.err.println(folder +" is not a folder.");
				System.exit(1);
			}
			if(!potentialFolder.canWrite())
			{
				System.err.println(folder +" can not be written with the current rights.");
				System.exit(1);
			}
			System.err.println("Option "+ args[i] +" is not managed yet.");
			return -1;
		}
		else if(args[i].equals("--list-episodes"))
		{
			listEpisodes = true;
		}
		else if(args[i].equals("--no-list-episodes"))
		{
			listEpisodes = false;
		}
		else
		{
			System.err.println("Argument "+ args[i] +" is not managed.");
			return -1;
		}
		return i;
	}
	
	private
	boolean
	setUnsafe(final String[] args)
	{
		for(int i=0; i < args.length; ++i)
		{
			i = parseArgument(args, i);
			if(i < 0)
			{
				return false;
			}
		}
		
		return true;
	}
	
	public
	boolean
	set(final String[] args)
	{
		if(args == null)
		{
			return false;
		}
		return setUnsafe(args);
	}
	
	
	public
	void
	ask()
	{
		final Scanner scanner = new Scanner(System.in);
		downloader.askAndSetLanguage(scanner);
		episodeNumber = EpisodeNumber.askAndGetACorrectOne(scanner);
		scanner.close();
	}
	
	public
	void
	askIfNeeded()
	{
		if(ask)
		{
			ask();
		}
	}
	
	
	public
	boolean
	isValidEpisodeNumber(final PrintStream errorStream)
	{
		if(episodeNumber < 0 && episodeNumber != EpisodeNumber.ALL)
		{
			if(errorStream != null)
			{
				errorStream.println(
						"You asked episode "+ episodeNumber + ", "+
						"but it is a negative number!");
				errorStream.flush();
			}
			return false;
		}
		if(episodeNumber >= EpisodesProxy.size())
		{
			if(errorStream != null)
			{
				errorStream.println(
						"You asked episode "+ episodeNumber + ", "+
								"but only "+ EpisodesProxy.size() +" were found.");
				errorStream.flush();
			}
			return false;
		}
		return true;
	}
	
	public
	boolean
	isValidEpisodeNumber()
	{
		return isValidEpisodeNumber(System.err);
	}
	
	public
	void
	exitIfInvalidEpisodeNumber()
	{
		if(!isValidEpisodeNumber())
		{
			System.exit(1);
		}
	}
}
