/*
 * Copyright (C) 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot;


public final class EpisodesProxy
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private EpisodesProxy()
	{}
	
	
	private static Episode[] _episodes = null;
	
	
	private static
	void
	fetchOnly()
	{
		if(_episodes == null)
		{
			_episodes = Episodes.getArrayFromDefaultUrl();
		}
	}
	
	private static
	int
	sizeWithoutFetching()
	{
		if(_episodes == null)
		{
			return 0;
		}
		return _episodes.length;
	}
	
	
	public static
	boolean
	fetch()
	{
		fetchOnly();
		return sizeWithoutFetching() > 0;
	}
	
	public static
	void
	fetchOrExit()
	{
		if(_episodes == null)
		{
			_episodes = Episodes.getArrayFromDefaultUrlOrExit();
		}
	}
	
	public static
	int
	size()
	{
		fetchOnly();
		return sizeWithoutFetching();
	}
	
	public static
	Episode[]
	getAll()
	{
		fetch();
		return _episodes;
	}
	
	public static
	Episode
	getOne(int index)
	{
		fetch();
		if(index < 0 || index >= size())
		{
			return null;
		}
		return _episodes[index];
	}
}
