/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public final class EpisodeUtilsTests
{
	private static final String[][] EPISODES_STRINGS = {
		{
			"ep01_Potion-of-Flight",
			"Potion-of-Flight"
		},
		{
			"ep02_Rainbow-potions",
			"Rainbow-potions"
		},
		{
			"ep03_The-secret-ingredients",
			"The-secret-ingredients"
		},
		{
			"ep04_Stroke-of-genius",
			"Stroke-of-genius"
		},
		{
			"ep05_Special-holiday-episode",
			"Special-holiday-episode"
		},
		{
			"ep06_The-Potion-Contest",
			"The-Potion-Contest"
		},
		{
			"ep07_The-Wish",
			"The-Wish"
		},
		{
			"ep08_Pepper-s-Birthday-Party",
			"Pepper-s-Birthday-Party"
		},
		{
			"ep09_The-Remedy",
			"The-Remedy"
		},
		{
			"ep10_Summer-Special",
			"Summer-Special"
		},
		{
			"ep11_The-Witches-of-Chaosah",
			"The-Witches-of-Chaosah"
		},
		{
			"ep12_Autumn-Clearout",
			"Autumn-Clearout"
		},
		{
			"ep13_The-Pyjama-Party",
			"The-Pyjama-Party"
		},
		{
			"ep14_The-Dragon-s-Tooth",
			"The-Dragon-s-Tooth"
		},
		{
			"ep15_The-Crystal-Ball",
			"The-Crystal-Ball"
		},
		{
			"ep16_The-Sage-of-the-Mountain",
			"The-Sage-of-the-Mountain"
		},
		{
			"ep17_A-Fresh-Start",
			"A-Fresh-Start"
		},
		{
			"ep18_The-Encounter",
			"The-Encounter"
		},
		{
			"ep19_Pollution",
			"Pollution"
		},
		{
			"ep20_The-Picnic",
			"The-Picnic"
		},
		{
			"ep21_The-Magic-Contest",
			"The-Magic-Contest"
		}
	};
	
	
	public static
	void
	parseDefault(final String id, int number, final String name)
	{
		final Episode ep = EpisodeUtils.parseDefault(id);
		assertNotNull(ep);
		assertEquals(ep.getNumber(), number);
		assertEquals(ep.getName(),   name);
		assertTrue(ep.isValid());
	}
	
	@Test
	public void
	parseDefaultOk()
	{
		for(int i=0; i < EPISODES_STRINGS.length; ++i)
		{
			parseDefault(EPISODES_STRINGS[i][0], i+1, EPISODES_STRINGS[i][1]);
		}
	}
	
	@Test
	public void parseDefaultFail()
	{
		assertNull(EpisodeUtils.parseDefault(null));
		assertNull(EpisodeUtils.parseDefault(""));
		assertNull(EpisodeUtils.parseDefault("ep"));
		assertNull(EpisodeUtils.parseDefault("ep1"));
		assertNull(EpisodeUtils.parseDefault("ep01"));
		assertNull(EpisodeUtils.parseDefault("ep01_"));
	}
}
