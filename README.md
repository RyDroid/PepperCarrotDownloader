# A free/libre program to download episodes of Pepper & Carrot

It is a program to download episodes of [Pepper & Carrot](http://www.peppercarrot.com/).
Pepper & Carrot is made by [David Revoy](https://www.davidrevoy.com/).
It is [free/libre software](https://www.gnu.org/philosophy/free-sw.html).
It is written in [Java](https://en.wikipedia.org/wiki/Java_%28programming_language%29) (and works with [OpenJDK](http://openjdk.java.net/)).
It currently only has a command line interface.
Happy hacking and enjoy!

## Build

You need a [JDK (Java Development Kit)](https://fr.wikipedia.org/wiki/Java_Development_Kit).
On at least Debian, you can install OpenJDK with `apt-get install default-jdk` (with root/SuperUser rights).
It probably also works on systems based on Debian, like Trisquel, gNewSense, Ubuntu and Mint.

You can compile with [Apache Ant](https://ant.apache.org/).
On at least Debian, you can install it with `apt-get install ant` (with root/SuperUser rights).
It probably also works on systems based on Debian, like Trisquel, gNewSense, Ubuntu and Mint.
Then execute the command `ant`.

## Modify

You can import Java code in your favorite EDI if it can import a build of Apache Ant (like Eclipse).

All text files have to be in [UTF-8](https://en.wikipedia.org/wiki/UTF-8) (without [BOM](https://en.wikipedia.org/wiki/Byte_order_mark)) encoding (generally UTF-8 on GNU/Linux and [ISO 8859-15](https://en.wikipedia.org/wiki/ISO/IEC_8859-15) on MS Windows) and LF [character newline](https://en.wikipedia.org/wiki/Newline) (sometime called UNIX newline/endline) (Windows generally used CRLF).
Unfortunately, by default some text editors save with the same encoding and character newline of your OS.

An empty line must be at the end of text files.
The aim is readability with [`cat file`](https://en.wikipedia.org/wiki/Cat_%28Unix%29).

We use english, so please use it everywhere in the project (messages, function names, doc, etc).

Names of variables, functions/methods, classes and everything else have to be clear, even if the name is a little longer.
Names of variables should not be a name of a type.

As much as possible, the source code should be a good documentation.
Otherwise, you have to create documentation.

Try to write unit tests (with [JUnit](http://junit.org/)).
It does not take a lot of time and is good for quality and maintenance.
Moreover, tests can be considered as documentation.

If you need to make a structured document, you should consider [Markdown](https://en.wikipedia.org/wiki/Markdown).
For example, this document uses the Markdown syntax.
For longer texts or presentations, [LaTeX](http://latex-project.org/) and HTML+CSS(+JS) could be good options.

You must release your contributions under free/libre license(s) if you want them to be accepted upstream.

### Things to do

- Add the option `--base-url`.
- Manage the `--format` option.
  [PDF and ePub are available](http://www.freesmug.org/peppercarrot).
- Option not to download again.
  `-f`, `--force` and `--force-download` to force to download again.
- Option to define folder to put content.
  Options could be `-o`, `--output`, and `--folder`.
- Option to download only a part (`--part`).
- Option to download only a range.
  For example: `-e 1-4`, `--part 1-5`.
- Create a rule for ant or make to install the program.
- Create a GUI (with Spring or OpenJFX).
- Add unit tests.
- Create packages (deb, rpm, dmg, etc).

## Get newer versions

You may find a new version with [the repository rydroid/PepperCarrotDownloader on GitLab.com](https://gitlab.com/RyDroid/PepperCarrotDownloader).

## License and authors

See [LICENSE.md](LICENSE.md) and logs of git for the full list of contributors of the project.
