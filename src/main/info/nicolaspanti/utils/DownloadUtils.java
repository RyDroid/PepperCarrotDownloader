/*
 * Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.utils;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;


public final class DownloadUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	DownloadUtils()
	{}
	
	
	public static
	boolean
	downloadFileWithUrl(final URL url, final String filePathToSave)
	{
		try
		{
			final ReadableByteChannel rbc = Channels.newChannel(url.openStream());
			final FileOutputStream fos = new FileOutputStream(filePathToSave);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
		}
		catch(final FileNotFoundException e)
		{
			return false;
		}
		catch (final IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static
	boolean
	downloadFileWithUrlString(final String url, final String filePathToSave)
	{
		try
		{
			return downloadFileWithUrl(new URL(url), filePathToSave);
		}
		catch(final MalformedURLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public static
	boolean
	downloadFileWithUrlString(final String url)
	{
		return downloadFileWithUrlString(url, UrlUtils.getFileName(url));
	}
	
	public static
	boolean
	downloadFileInFolderWithUrlString(final String url, final File potentialDirectory)
	{
		if(
				potentialDirectory == null ||
				!potentialDirectory.isDirectory() ||
				!potentialDirectory.canWrite())
		{
			return false;
		}
		final String filePathToSave =
				potentialDirectory.getAbsolutePath() +"/"+ UrlUtils.getFileName(url);
		return downloadFileWithUrlString(url, filePathToSave);
	}
	
	public static
	boolean
	downloadFileInFolderWithUrlString(final String url, final String folderPath)
	{
		if(folderPath == null || folderPath.isEmpty())
		{
			return false;
		}
		final File potentialDirectory = new File(folderPath);
		return downloadFileInFolderWithUrlString(url, potentialDirectory);
	}
}
