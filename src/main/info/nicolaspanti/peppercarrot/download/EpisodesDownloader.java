/*
 * Copyright (C) 2016-2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot.download;


import info.nicolaspanti.peppercarrot.Episode;
import info.nicolaspanti.peppercarrot.EpisodeNumber;
import info.nicolaspanti.peppercarrot.EpisodesProxy;
import info.nicolaspanti.utils.CommandLineArgumentsUtils;

import java.io.PrintStream;


public final class EpisodesDownloader
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private EpisodesDownloader()
	{}
	
	
	private static
	short
	downloadOne(final EpisodeDownloader downloader, short episodeNumber)
	{
		final short nbImages =
				downloader.download(EpisodesProxy.getOne(episodeNumber -1));
		if(nbImages < 0)
		{
			final String message =
					nbImages +" image(s) downloaded for episode "+ episodeNumber +"!";
			System.err.println(message);
		}
		else
		{
			System.out.print(nbImages +" image");
			if(nbImages < 2)
			{
				System.out.print(" was");
			}
			else
			{
				System.out.print("s were");
			}
			System.out.println(" downloaded for episode "+ episodeNumber +".");
		}
		return nbImages;
	}
	
	private static
	short
	downloadAll(final EpisodeDownloader downloader)
	{
		short nbImagesAll = 0;
		for(
				short episodeNumber = 1;
				episodeNumber < EpisodesProxy.size();
				++episodeNumber)
		{
			System.out.println("Try to download episode "+ episodeNumber +"…");
			final short nbImages = downloadOne(downloader, episodeNumber);
			nbImagesAll += nbImages;
			if(nbImages < 1)
			{
				System.out.println("This episode apparently does not exist yet.");
				return (short) - nbImagesAll;
			}
		}
		return nbImagesAll;
	}
	
	private static
	short
	download(final EpisodeDownloader downloader, short episodeNumber)
	{
		if(episodeNumber == EpisodeNumber.ALL)
		{
			return downloadAll(downloader);
		}
		return downloadOne(downloader, episodeNumber);
	}
	
	private static
	short
	download(final InitializationParameters initParams)
	{
		if(initParams == null)
		{
			return -1;
		}
		return download(initParams.downloader, initParams.episodeNumber);
	}
	
	private static
	boolean
	printListOfEpisodes()
	{
		if(EpisodesProxy.size() == 0)
		{
			System.err.println("No episode found.");
			return false;
		}
		
		for(final Episode episode : EpisodesProxy.getAll())
		{
			System.out.println(
					String.format("%3d", episode.getNumber())
					+ episode.getName());
		}
		return true;
	}
	
	
	public static
	void
	printHelpGeneral(final PrintStream stream)
	{
		stream.println("General options:");
		stream.println("");
		stream.println("  -h --help");
		stream.println("      Prints this message and exits.");
		stream.println("  --version");
		stream.println("      Prints the version of this program and exits.");
		stream.println("  --quiet");
		stream.println("      Puts the program in quiet mode.");
		stream.println("  --verbose");
		stream.println("      Puts the program in verbose mode.");
		stream.println("  --ask");
		stream.println("      Asks questions with a prompt.");
		stream.println("  --no-ask");
		stream.println("      Does not ask questions with a prompt.");
	}
	
	public static
	void
	printHelpDownload(final PrintStream stream)
	{
		stream.println("Downloading options:");
		stream.println("  -e --episode");
		stream.println("      Defines the episode to download.");
		stream.println("      "+ EpisodeNumber.ALL +" to download them all");
		stream.println("      or --all-episodes.");
		stream.println("  --all-episodes");
		stream.println("      Download all episodes.");
		stream.println("  --language value");
		stream.println("      Defines the value of the language.");
		stream.println("  -q --quality value");
		stream.println("      Defines the value of the quality.");
		stream.println("  -f --format value");
		stream.println("      Defines the desired format.");
	}
	
	public static
	void
	printHelpOthers(final PrintStream stream)
	{
		stream.println("Other options:");
		stream.println("  --list-episodes");
		stream.println("      Prints the list of episodes.");
		stream.println("  --no-list-episodes");
		stream.println("      Does not print the list of episodes.");
	}
	
	public static
	void
	printHelp(final PrintStream stream)
	{
		if(stream != null)
		{
			printHelpGeneral (stream);
			printHelpDownload(stream);
			printHelpOthers  (stream);
		}
	}
	
	public static
	void
	printHelp()
	{
		printHelp(System.out);
	}
	
	
	public static
	void
	main(final String[] args)
	{
		final InitializationParameters initParams =
				new InitializationParameters();
		if(args.length > 0)
		{
			if(CommandLineArgumentsUtils.isHelpArgument(args[0]))
			{
				printHelp();
				System.exit(0);
			}
			if(args[0].equals("--version"))
			{
				System.out.println("Version 1.0.0");
				System.exit(0);
			}
			
			if(!initParams.set(args))
			{
				System.err.println("An error occured while parsing argument(s).");
				System.exit(1);
			}
		}
		
		if(initParams.listEpisodes)
		{
			printListOfEpisodes();
		}
		else
		{
			initParams.askIfNeeded();
			initParams.exitIfInvalidEpisodeNumber();
			if(download(initParams) < 0)
			{
				System.exit(1);
			}
		}
	}
}
