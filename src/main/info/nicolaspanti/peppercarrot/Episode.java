/*
 * Copyright (C) 2016-2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package info.nicolaspanti.peppercarrot;


public class Episode
{
	private short  _number;
	private String _name;
	
	
	public Episode(short number, final String name)
	{
		_number = number;
		_name   = name;
	}
	
	public Episode(int number, final String name)
	{
		this((short) number, name);
	}

	
	public
	short
	getNumber()
	{
		return _number;
	}
	
	public
	String
	getName()
	{
		return _name;
	}
	
	public
	boolean
	isValid()
	{
		return _number >= 0 && _name != null && _name.length() > 0;
	}
	
	
	private
	String
	toIdentifierUnsafe(
			final String beforeNumber, final String betweenNumberAndName, byte nbDigits)
	{
		if(
				(nbDigits == 1 && _number >=    10) ||
				(nbDigits == 2 && _number >=   100) ||
				(nbDigits == 3 && _number >=  1000) ||
				(nbDigits == 4 && _number >= 10000))
		{
			return null;
		}
		
		return
				beforeNumber +
				String.format("%0"+ nbDigits +"d", _number)
				+ betweenNumberAndName + _name;
	}
	
	public
	String
	toIdentifier(final String beforeNumber, final String betweenNumberAndName, byte nbDigits)
	{
		if(beforeNumber == null || betweenNumberAndName == null || nbDigits == 0)
		{
			return null;
		}
		return toIdentifierUnsafe(beforeNumber, betweenNumberAndName, nbDigits);
	}
	
	public
	String
	toIdentifierWithDefaultNdDigits(
			final String beforeNumber, final String betweenNumberAndName)
	{
		return toIdentifier(beforeNumber, betweenNumberAndName, (byte) 2);
	}
	
	public
	String
	toDefaultIdentifier()
	{
		return toIdentifierWithDefaultNdDigits("ep", "_");
	}
}
